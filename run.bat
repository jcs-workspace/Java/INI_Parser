:: ========================================================================
:: $File: run.bat $
:: $Date: 2018-02-12 17:54:50 $
:: $Revision: $
:: $Creator: Jen-Chieh Shen $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright � 2018 by Shen, Jen-Chieh $
:: ========================================================================


set CLASSPATH=.;dist\*;bin\

java INI_Parser_Test
