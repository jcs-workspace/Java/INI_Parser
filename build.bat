:: ========================================================================
:: $File: build.bat $
:: $Date: 2018-02-12 17:53:59 $
:: $Revision: $
:: $Creator: Jen-Chieh Shen $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright � 2018 by Shen, Jen-Chieh $
:: ========================================================================


set CLASSPATH=.;dist\*;bin\

javac -d bin/ src/INI_Parser_Test.java
